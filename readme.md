<div align="center">
    <h1>
        <img src="repo_assets/nixos_logo.png" width="96"><br>
        thom's nixos config. files
    </h1>
</div>

![screenshot](repo_assets/desktop_screenshot.png)

This repo contains my configuration files for nixos, including hardware-specific settings for whatever my primary machine is right now

Overview
--------

This configuration is intended to be as minimal and [free](https://www.gnu.org/philosophy/free-sw.en.html) as is practical.

- The system is Wayland-based and uses [river](https://github.com/riverwm/river) as a compositor and for window management
- The rest of the desktop environment consists of [i3status-rust](https://github.com/greshake/i3status-rust) with an '[adapter](https://github.com/MaxVerevkin/i3bar-river)' for river and [fuzzel](https://codeberg.org/dnkl/fuzzel) to select applications and launch custom shortcut scripts
- Basic tasks like text editing and file management are handled using minimal - usually terminal-based - applications ([helix](https://github.com/helix-editor/helix) and [yazi](https://github.com/sxyazi/yazi/) respectively)
- Packages are declared in the system/user configuration and all come from the [Nixpkgs](https://github.com/NixOS/nixpkgs) repository
- Configuration files are managed using [Home Manager](https://github.com/nix-community/home-manager)
- [librewolf](https://codeberg.org/librewolf) is used to strike a balance between privacy and functionality when browsing the web
- Other software used is listed in the `packages.nix` file for each configuration

Installation
------------

The easiest way to get started with NixOS is to use one of their graphical ISO images to [install](https://nixos.org/download.html) it on your system. There's no need to install a graphical desktop environment during the GUI installation as one is provided by this configuration.

Once NixOS is installed, install the packages necessary to get started with this configuration:

``` bash
nix-env -iA nixos.git nixos.home-manager
```

Clone this repository somewhere you'd like to keep your configuration files:

``` bash
mkdir ~/.config
cd ~/.config
git clone https://gitlab.com/thom-cameron/nixos
```

Replace the `hardware-configuration.nix` file from this repository with the one that was generated for your machine during the NixOS installation process. For example:

``` bash
sudo cp /etc/nixos/hardware-configuration.nix ~/.config/nixos/system/lat/hardware-configuration.nix 
```

You should also make any other modifications that you'd like to at this point. For example, changing the hostname or replacing my name with yours. Most instances of personal information derive from the "user" collection in the `flake.nix`, so this should be pretty convenient. 

Finally, run the following commands to configure the system and user respectively:

``` bash
sudo nixos-rebuild switch --flake "~/.config/nixos/#nix-lat"
home-manager switch --flake "~/.config/nixos/#thom@nix-lat"
```

Reboot the system and enjoy. 

Disclaimer
----------

I'm _fairly_ new to this, so please don't assume anything here is done right.
