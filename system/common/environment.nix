{
  # set system-wide environment variables
  environment.variables = {
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_CACHE_HOME = "$HOME/.cache";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_STATE_HOME = "$HOME/.local/state";
    XDG_DOCUMENTS_DIR = " $HOME/documents";
    XDG_DOWNLOAD_DIR = " $HOME/downloads";
    XDG_MUSIC_DIR = " $HOME/music";
    XDG_PICTURES_DIR = " $HOME/pictures";
    XDG_SCREENSHOTS_DIR = "$HOME/pictures/screenshots";

    FLAKE = "$HOME/.config/nixos";
    SHORTCUTS_DIR = "$HOME/.local/bin/shortcuts";
    CONFIG_SCRIPTS_DIR = "$HOME/.local/bin/config_scripts";
    NB_DIR = "$HOME/documents/notes";
  };
}
