{ user, ... }:
{
  programs.git = {
    enable = true;

    userName = user.fullName;
    userEmail = user.email;

    extraConfig = {
      credential = {
        helper = "store";
      };

      safe = {
        directory = "/home/${user.name}/.config/nixos";
      };
    };
  };
}
