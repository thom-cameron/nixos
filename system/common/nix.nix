{ pkgs, ... }:
{
  nix = {
    package = pkgs.nixVersions.latest;

    extraOptions = # conf
      ''
        experimental-features = nix-command flakes
      '';
  };

  programs.nh = {
    enable = true;
    clean.enable = true;
  };
}
