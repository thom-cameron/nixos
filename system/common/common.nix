{
  imports = [
    ./audio.nix
    ./boot.nix
    ./environment.nix
    ./locale.nix
    ./nix.nix
    ./packages.nix
    ./shell.nix
    ./security.nix
  ];
}
