{ pkgs, ... }:
{
  # disable nonfree software by default
  nixpkgs.config = {
    allowUnfree = false;
    allowInsecure = false;
  };

  # system-wide packages
  environment.systemPackages = with pkgs; [
    bash
    wget
    git
    nh
    helix
    yazi
    ripgrep
    bottom
  ];
}
