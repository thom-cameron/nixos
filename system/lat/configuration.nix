{
  system.stateVersion = "24.05";

  imports = [
    # common
    ../common/common.nix

    # system-specific
    ./devices/devices.nix
    ./environment/environment.nix
    ./media/media.nix
    ./hardware-configuration.nix
    ./packages.nix
    ./power.nix
    ./users.nix
  ];
}
