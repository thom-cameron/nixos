{
  pkgs,
  lib,
  ...
}:
{
  # allow limited nonfree software
  nixpkgs.config = {
    allowUnfreePredicate =
      pkg:
      builtins.elem (lib.getName pkg) [
        "steam"
        "steam-original"
        "steam-unwrapped"
        "steam-run"
        "corefonts"
        "vista-fonts"
      ];
  };

  # system-wide packages
  environment.systemPackages = with pkgs; [
    # environment
    greetd.tuigreet
    corefonts
    vistafonts
    alacritty
    fuzzel
    pinentry-qt

    # utilities
    helix
    bat
    xdg-utils

    # web
    librewolf

    # media
    steam
    # virtualbox # breaks kernel driver
  ];
}
