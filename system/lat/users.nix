{
  pkgs,
  user,
  ...
}:
{
  # create user account
  users.users = {
    "${user.name}" = {
      isNormalUser = true;
      description = user.fullName;
      extraGroups = [
        "wheel"
        "input"
      ];
      shell = pkgs.nushell;
    };
  };
}
