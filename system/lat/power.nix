{
  services.logind.extraConfig = ''
    # configure power button beheviour
    HandlePowerKey=suspend
    HandlePowerKeyLongPress=poweroff
  '';

  # power management
  services.thermald.enable = true;
  services.tlp.enable = true;
}
