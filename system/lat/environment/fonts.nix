{ pkgs, ... }:
{
  # install system fonts
  fonts = {
    fontDir.enable = true;
    packages = with pkgs; [
      nerd-fonts.caskaydia-cove
      openmoji-color
      inter
      corefonts
      vistafonts
    ];
  };
}
