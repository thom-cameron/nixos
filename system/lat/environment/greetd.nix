{ pkgs, ... }:
{
  # use greetd and tuigreet as a display manager
  services.greetd = {
    enable = true;
    settings.default_session = {
      command = ''
        ${pkgs.greetd.tuigreet}/bin/tuigreet \
          --remember \
          --time \
          --asterisks \
          --cmd river
      '';
    };
  };
}
