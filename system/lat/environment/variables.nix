{ pkgs, ... }:
{
  environment.variables = {
    XDG_CURRENT_DESKTOP = "river";

    EDITOR = "${pkgs.helix}/bin/hx";
    TERMINAL = "${pkgs.alacritty}/bin/alacritty";
    PAGER = "${pkgs.bat}/bin/bat --style=header-filename,header-filesize,grid";
    OPENER = "${pkgs.xdg-utils}/bin/xdg-open";
    BROWSER = "${pkgs.librewolf}/bin/librewolf";
    MENU = "${pkgs.fuzzel}/bin/fuzzel -d";
  };
}
