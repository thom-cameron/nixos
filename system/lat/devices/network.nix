{ ... }:
{
  networking.hostName = "nix-lat";

  # internet connectivity
  networking.wireless.iwd.enable = true;

  # bluetooth
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };
  services.blueman.enable = true;
}
