{ pkgs, ... }:
let
  webcam_settings = builtins.concatStringsSep "," [
    "brightness=110"
    "auto_exposure=3"
    "zoom_absolute=32"
  ];
  webcam_udev_rule = builtins.concatStringsSep ", " [
    ''SUBSYSTEM=="video4linux"''
    ''KERNEL=="video[0-9]*"''
    ''ATTR{index}=="0"''
    ''ATTRS{idVendor}=="045e"''
    ''ATTRS{idProduct}=="0840"''
    ''RUN+="${pkgs.v4l-utils}/bin/v4l2-ctl -d $devnode --set-ctrl=${webcam_settings}"''
  ];
in
{
  # custom webcam settings
  services.udev.extraRules = ''
    ${webcam_udev_rule}
  '';
}
