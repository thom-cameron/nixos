{
  imports = [
    ./disks.nix
    ./network.nix
    ./printing.nix
    ./udev-rules.nix
  ];
}
