{
  pkgs,
  lib,
  ...
}:
{
  nixpkgs.config = {
    allowUnfree = false;
    allowUnfreePredicate =
      pkg:
      builtins.elem (lib.getName pkg) [
        "steam"
        "steam-original"
        "steam-unwrapped"
        "osu-lazer-bin"
      ];

    allowInsecure = false;
  };

  home.packages = with pkgs; [
    # environment
    river
    i3bar-river
    wideriver
    swaybg
    xdg-utils
    alacritty
    mako
    libnotify
    wl-clipboard
    fuzzel
    i3status-rust
    wf-recorder
    pamixer
    playerctl
    alsa-utils
    ncpamixer
    helvum
    brightnessctl
    darkman
    wl-gammarelay-rs
    kanshi
    wdisplays
    swayidle
    hyprpicker
    lswt
    udiskie
    iwgtk
    dconf
    gnome-themes-extra
    adwaita-icon-theme

    # utility
    nh
    file
    bat
    eza
    zoxide
    fd
    pv
    fzf
    yazi
    fastmod
    moreutils
    delta
    gitui
    unzip
    watchexec
    du-dust
    chafa
    grim
    slurp
    tesseract
    gnome-disk-utility
    xfce.xfburn
    nautilus
    unipicker
    pipes
    fastfetch
    powertop
    # isw # not in nixpkgs

    # web
    # librewolf # collides with the one the home-manager config installs
    ungoogled-chromium
    signal-desktop
    tor-browser
    protonvpn-gui

    # media
    loupe
    ffmpeg
    # mpv # collides with the one the home-manager config installs
    tidal-hifi
    qbittorrent
    nicotine-plus
    steam
    osu-lazer-bin

    # office
    # pandoc # collides with the one the home-manager config installs
    nb
    marksman
    tinymist
    harper
    libreoffice
    hunspellDicts.en-gb-large
    papers
    slides
    typst
    anki
    teams-for-linux

    # graphics
    inkscape
    gimp

    # recording
    musescore
    audacity
    ardour
    hydrogen
    lsp-plugins
    qjackctl
    cardinal

    # programming
    nil
    nixfmt-rfc-style
    (python3.withPackages (ps: with ps; [ ipython ]))
    poetry
    pyright
    ruff
    ruff-lsp
    clang
    cargo
    rustc
    rust-analyzer
    clippy
    rustfmt
    evcxr
    lldb
    vscode-langservers-extracted
    nodePackages_latest.bash-language-server
    stylua
  ];
}
