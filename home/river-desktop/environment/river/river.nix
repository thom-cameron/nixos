{
  lib,
  config,
  pkgs,
  theme,
  ...
}:
let
  super = "Super";

  layout_options = builtins.concatStringsSep " " [
    "--layout left"
    "--stack dwindle"
    "--count-master 1"
    "--ratio-master 0.5"
    "--border-width 4"
    "--border-width-monocle 4"
    "--inner-gaps ${toString theme.desktop.padding}"
    "--outer-gaps 0"
    "--no-smart-gaps"
    ''--border-color-focused "0x${theme.accent.primary}"''
    ''--border-color-focused-monocle "0x${theme.accent.tertiary}"''
    ''--border-color-unfocused "0x${theme.background.tertiary}"''
  ];

  input = import ./input.nix;
  keymap = import ./keymap.nix { inherit pkgs super; };
in
{
  options.personal.environment.river.enable = lib.mkEnableOption "enable river";

  config = lib.mkIf config.personal.environment.river.enable {
    wayland.windowManager.river = {
      enable = true;

      extraSessionVariables = {
        all_tags = "$(((1 << 32) - 1))";
      };

      settings =
        input
        // keymap
        // {
          # layout
          default-layout = "wideriver";
          default-attach-mode = "below";

          # theming
          border-width = 3;
          background-color = "0x${theme.background.primary}";
          border-color-focused = "0x${theme.accent.primary}";
          border-color-urgent = "0x${theme.colours.normal.red}";
          border-color-unfocused = "0x${theme.background.secondary}";
          xcursor-theme = "${theme.cursor.name} ${toString theme.cursor.size}";

          # rules
          rule-add = [
            "ssd"

            {
              "-title" = [
                {
                  "Picture-in-Picture" = [
                    "float"
                    { tags = "$all_tags"; }
                    { position = "10000 10000"; }
                    { dimensions = "640 360"; }
                  ];
                }
              ];
            }
          ];

          # startup programs
          spawn = [
            # environment
            '''wideriver ${layout_options}' ''
            "i3bar-river"
            "kanshi"
            '''wl-gammarelay-rs run' ''
            '''swaybg -c ${theme.background.primary}' ''

            # config scripts
            ''"$CONFIG_SCRIPTS_DIR/empty_trash"''

            # startup apps
            "librewolf"
            "tidal-hifi"
            "signal-desktop"
          ];
        };

      extraConfig = # bash
        ''
          # tags and selection
          for i in $(seq 1 9)
          do
              tags=$((1 << ($i - 1)))
              riverctl map normal ${super} $i set-focused-tags $tags
              riverctl map normal ${super}+Shift $i set-view-tags $tags
              riverctl map normal ${super}+Control $i toggle-focused-tags $tags
              riverctl map normal ${super}+Shift+Control $i toggle-view-tags $tags
          done
        '';
    };
  };
}
