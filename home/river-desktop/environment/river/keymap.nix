{ pkgs, super }:
{
  map.normal = {
    # environment shortcuts
    "${super} P" = ''spawn 'exec $(${pkgs.fuzzel}/bin/fuzzel)' '';
    "${super} S" = ''spawn "$SHORTCUTS_DIR/shortcut"'';
    "${super} Escape" = ''spawn "systemctl suspend"'';

    # app shortcuts
    "${super}+Shift Return" = ''spawn $TERMINAL'';
    "${super}+Shift B" = ''spawn $BROWSER'';
    "${super}+Shift F" = ''spawn "$TERMINAL -e yazi"'';
    "${super}+Shift D" = ''spawn "$SHORTCUTS_DIR/terminal_at_directory"'';
    "${super}+Shift N" = ''spawn "$TERMINAL -e nb new"'';
    "${super}+Shift C" = ''spawn "$TERMINAL -e ipython"'';
    "${super}+Shift S" = ''spawn "$TERMINAL -e ncpamixer -t output"'';
    "${super}+Shift P" = ''spawn "playerctl play-pause"'';
    "${super}+Shift E" = ''spawn "$TERMINAL -e $EDITOR ~"'';
    "${super}+Shift W" = ''spawn iwgtk'';
    "${super}+Shift Escape" = ''spawn "$TERMINAL -e btm"'';

    # modifying views
    "${super} Q" = "close";
    "${super} Space" = "toggle-float";
    "${super}+Shift Space" = ''spawn "$SHORTCUTS_DIR/picture-in-picture"'';
    "${super} F" = "toggle-fullscreen";
    "${super} B" = ''spawn "pkill -SIGUSR1 i3bar-river"''; # hide bar

    # layout commands
    "${super} Equal" = ''send-layout-cmd wideriver "--ratio +0.05"'';
    "${super} Minus" = ''send-layout-cmd wideriver "--ratio -0.05"'';
    "${super}+Shift Equal" = ''send-layout-cmd wideriver "--count +1"'';
    "${super}+Shift Minus" = ''send-layout-cmd wideriver "--count -1"'';
    "${super} Left" = ''send-layout-cmd wideriver "--layout left"'';
    "${super} Right" = ''send-layout-cmd wideriver "--layout right"'';
    "${super} Up" = ''send-layout-cmd wideriver "--layout monocle"'';
    "${super} Down" = ''send-layout-cmd wideriver "--layout top"'';

    # moving and focusing views
    "${super} J" = "focus-view next";
    "${super} K" = "focus-view previous";
    "${super}+Shift J" = "swap next";
    "${super}+Shift K" = "swap previous";
    "${super} L" = "focus-output next";
    "${super} H" = "focus-output previous";
    "${super}+Shift L" = "send-to-output -current-tags next";
    "${super}+Shift H" = "send-to-output -current-tags previous";
    "${super}+Shift+Control L" = "send-to-output next";
    "${super}+Shift+Control H" = "send-to-output previous";
    "${super} Return" = "zoom";

    # modifying and fousing tags
    "${super} 0" = "set-focused-tags $all_tags";
    "${super}+Shift 0" = "set-view-tags $all_tags";

    # media keys
    "None XF86AudioRaiseVolume" = ''spawn "pamixer -i 5"'';
    "None XF86AudioLowerVolume" = ''spawn "pamixer -d 5"'';
    "None XF86AudioMute" = ''spawn "pamixer --toggle-mute"'';
    "None XF86AudioMedia" = ''spawn "playerctl play-pause"'';
    "None XF86AudioPlay" = ''spawn "playerctl play-pause"'';
    "None XF86AudioPrev" = ''spawn "playerctl previous"'';
    "None XF86AudioNext" = ''spawn "playerctl next"'';
    "None XF86MonBrightnessUp" = ''spawn "brightnessctl s 10%+"'';
    "None XF86MonBrightnessDown" = ''spawn "brightnessctl s 10%-"'';
  };

  map-pointer.normal = {
    # modifying views
    "${super} BTN_LEFT" = "move-view";
    "${super} BTN_RIGHT" = "resize-view";
    "${super} BTN_MIDDLE" = "toggle-float";
  };
}
