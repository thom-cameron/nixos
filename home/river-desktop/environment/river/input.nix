let
  touchpad = "pointer-1160-4132-DELL0A21:00_0488:1024_Touchpad";
  mouse = "pointer-1133-16480-Logitech_MX_Master";

  keyboard_options = builtins.concatStringsSep "," [
    "grp:rctrl_rshift_toggle"
    "altwin:swap_alt_win"
    "caps:escape_shifted_capslock"
  ];
in
{
  input = {
    "${touchpad}" = {
      tap = true;
      scroll-method = "two-finger";
      natural-scroll = true;
    };

    "${mouse}" = {
      "natural-scroll" = false;
      "accel-profile" = "flat";
      "pointer-accel" = -0.25;
    };
  };
  set-cursor-warp = "on-output-change";
  keyboard-layout = ''-options "${keyboard_options}" "us,gb"'';
  set-repeat = "25 250";
}
