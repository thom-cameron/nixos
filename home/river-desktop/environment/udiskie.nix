{
  lib,
  config,
  ...
}:
{
  options.personal.environment.udiskie.enable = lib.mkEnableOption "enable udiskie";

  config = lib.mkIf config.personal.environment.udiskie.enable {
    services.udiskie = {
      enable = true;

      notify = true;
    };
  };
}
