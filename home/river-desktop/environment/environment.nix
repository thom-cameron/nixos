{
  imports = [
    ./cursor.nix
    ./darkman.nix
    ./desktop-portal.nix
    ./fuzzel.nix
    ./gtk.nix
    ./i3bar-river.nix
    ./i3status-rust.nix
    ./kanshi.nix
    ./mako.nix
    ./river/river.nix
    ./swayidle.nix
    ./udiskie.nix
    ./xsettingsd.nix
  ];
}
