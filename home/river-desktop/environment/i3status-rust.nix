{
  lib,
  config,
  theme,
  ...
}:
{
  options.personal.environment.i3status-rust.enable = lib.mkEnableOption "enable i3status-rust";

  config = lib.mkIf config.personal.environment.i3status-rust.enable {
    programs.i3status-rust = {
      enable = true;

      bars.bottom = {
        settings = {
          theme = {
            theme = "native";
            overrides = {
              idle_fg = "#${theme.colours.light.white}";
              idle_bg = "#${theme.background.secondary}"; # only required while tags don't have bg
              info_fg = "#${theme.colours.light.white}";
              info_bg = "#${theme.background.secondary}";
              good_bg = "#${theme.colours.normal.green}";
              warning_bg = "#${theme.colours.normal.red}";
            };
          };

          icons = {
            icons = "material-nf";
            overrides = {
              # hacky replacements for toggles
              cpu_boost_on = "󱃃";
              cpu_boost_off = "󱃂";
              disk_drive = "󱦠";
            };
          };
        };

        blocks = [
          {
            block = "music";
            format = ''{ $play $artist, "$title.str(max_w:80,rot_interval:0.5)" |}'';
            click = [
              {
                button = "left";
                cmd = "playerctl play-pause";
              }
              {
                button = "right";
                cmd = "playerctl stop";
              }
            ];
            merge_with_next = false;
          }

          {
            block = "weather";
            format = "{ $icon $temp |}";
            service = {
              name = "metno";
              coordinates = [
                "55.86"
                "-4.25"
              ];
              lang = "en";
            };
            click = [
              {
                button = "left";
                cmd = "$TERMINAL --hold -e curl v2d.wttr.in/gla?Fm";
              }
            ];
            merge_with_next = false;
          }

          {
            block = "battery";
            interval = 5;
            format = " $icon $percentage ";
            full_format = "";
            click = [
              {
                button = "left";
                cmd = "$TERMINAL -e btm --battery --default_widget_type battery -e";
              }
            ];
            merge_with_next = false;
          }

          # terminal
          {
            block = "custom";
            interval = "once";
            format = "  ";
            command = "";
            click = [
              {
                button = "left";
                cmd = "riverctl set-focused-tags '1000000000'; $TERMINAL";
              }
            ];
            merge_with_next = true;
          }

          # notifications
          {
            block = "toggle";
            format = " $icon ";
            command_state = "makoctl mode | rg do-not-disturb";
            command_on = "makoctl mode -a do-not-disturb";
            command_off = "makoctl mode -r do-not-disturb";
            icon_on = "bell-slash";
            icon_off = "bell";
            interval = 5;
            click = [
              {
                button = "right";
                cmd = "makoctl restore";
              }
            ];
            merge_with_next = true;
          }

          {
            block = "sound";
            format = "{ $icon |}";
            driver = "auto";
            show_volume_when_muted = true;
            headphones_indicator = true;
            max_vol = 100;
            click = [
              {
                button = "left";
                cmd = "$TERMINAL -e ncpamixer -t output";
              }
              {
                button = "right";
                cmd = "helvum";
              }
            ];
            merge_with_next = true;
          }

          # auto suspend
          {
            block = "toggle";
            format = " $icon ";
            command_state = "systemctl --user is-active swayidle | rg '^active'";
            command_on = "systemctl --user start swayidle";
            command_off = "systemctl --user stop swayidle";
            icon_on = "ping";
            icon_off = "disk_drive";
            interval = 5;
            merge_with_next = true;
          }

          # hue shift
          {
            block = "toggle";
            format = " $icon ";
            command_state = "echo $(timeout 0.1s wl-gammarelay-rs watch '{t}') | rg ${toString theme.temperature.dark}";
            command_on = "busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Temperature q ${toString theme.temperature.dark}";
            command_off = "busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Temperature q ${toString theme.temperature.light}";
            icon_on = "cpu_boost_on";
            icon_off = "cpu_boost_off";
            interval = 5;
            merge_with_next = true;
          }

          {
            block = "backlight";
            format = "{ $icon |}";
            missing_format = "";
            minimum = 0;
            merge_with_next = true;
          }

          # dark mode
          {
            block = "toggle";
            format = " $icon ";
            command_state = "darkman get | rg dark";
            command_on = "darkman set dark";
            command_off = "darkman set light";
            icon_on = "weather_moon";
            icon_off = "weather_sun";
            interval = 5;
            merge_with_next = true;
          }

          {
            block = "custom";
            interval = "once";
            format = " 󰂯 ";
            command = "";
            click = [
              {
                button = "left";
                cmd = "blueman-manager";
              }
              {
                button = "right";
                cmd = "bluetooth toggle";
              }
            ];
            merge_with_next = true;
          }

          {
            block = "net";
            interval = 5;
            format = "{ $icon  |}";
            click = [
              {
                button = "left";
                cmd = "iwgtk";
              }
            ];
            merge_with_next = false;
          }

          {
            block = "time";
            format = " $timestamp.datetime(f:'%a %d %b %I:%M:%S%P') ";
            interval = 1;
            click = [
              {
                button = "left";
                cmd = "$TERMINAL --hold -e cal -m";
              }
              {
                button = "right";
                cmd = "$TERMINAL -e isw";
              }
            ];
            merge_with_next = false;
          }

          {
            block = "privacy";
            driver = [ { name = "v4l"; } ];
            merge_with_next = false;
          }
        ];
      };
    };
  };
}
