{
  lib,
  config,
  theme,
  ...
}:
{
  options.personal.environment.cursor.enable = lib.mkEnableOption "configure cursor";

  config = lib.mkIf config.personal.environment.cursor.enable {
    home.pointerCursor = {
      package = theme.cursor.package;
      name = theme.cursor.name;
      size = theme.cursor.size;

      gtk.enable = true;
      x11 = {
        enable = true;
        defaultCursor = theme.cursor.name;
      };
    };
  };
}
