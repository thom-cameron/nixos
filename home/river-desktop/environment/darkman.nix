{
  lib,
  config,
  pkgs,
  theme,
  ...
}:
{
  options.personal.environment.darkman.enable = lib.mkEnableOption "enable darkman";

  config = lib.mkIf config.personal.environment.darkman.enable {
    services.darkman = {
      enable = true;

      settings = {
        lat = 55.86;
        lng = -4.25;
        usegeoclue = false;
      };

      lightModeScripts = {
        notify = # bash
          ''
            ${pkgs.libnotify}/bin/notify-send darkman "switched to light mode"
          '';
        gtk-theme = # bash
          ''
            ${pkgs.dconf}/bin/dconf write /org/gnome/desktop/interface/color-scheme "'prefer-light'"
          '';
        gamma = # bash
          ''
            busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Temperature q ${toString theme.temperature.light}
          '';
      };
      darkModeScripts = {
        notify = # bash
          ''
            ${pkgs.libnotify}/bin/notify-send darkman "switched to dark mode"
          '';
        gtk-theme = # bash
          ''
            ${pkgs.dconf}/bin/dconf write /org/gnome/desktop/interface/color-scheme "'prefer-dark'"
          '';
        gamma = # bash
          ''
            busctl --user set-property rs.wl-gammarelay / rs.wl.gammarelay Temperature q ${toString theme.temperature.dark}
          '';
      };
    };
  };
}
