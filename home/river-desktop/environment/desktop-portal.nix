{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.environment.desktop-portal.enable = lib.mkEnableOption "configure desktop-portal";

  config = lib.mkIf config.personal.environment.desktop-portal.enable {
    xdg.portal = {
      enable = true;

      extraPortals = with pkgs; [
        xdg-desktop-portal-gtk
        xdg-desktop-portal-wlr
      ];

      config = {
        common = {
          default = "gtk";
          "org.freedesktop.impl.portal.Screenshot" = "wlr";
          "org.freedesktop.impl.portal.ScreenCast" = "wlr";
        };
      };
    };

    home.file.".config/xdg-desktop-portal-wlr/config" = {
      text = # ini
        ''
          [screencast]
          max_fps=30
          exec_before=${pkgs.mako}/bin/makoctl mode -a do-not-disturb
          exec_after=${pkgs.mako}/bin/makoctl mode -r do-not-disturb
        '';
    };
  };
}
