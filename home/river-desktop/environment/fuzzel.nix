{
  lib,
  config,
  pkgs,
  theme,
  ...
}:
{
  options.personal.environment.fuzzel.enable = lib.mkEnableOption "enable fuzzel";

  config = lib.mkIf config.personal.environment.fuzzel.enable {
    programs.fuzzel = {
      enable = true;

      settings = {
        main = {
          layer = "overlay";
          terminal = "${pkgs.alacritty}/bin/alacritty";

          width = 45;
          horizontal-pad = 12;
          image-size-ratio = 0.25;

          font = "${theme.font.mono.name}:size=13";
          dpi-aware = false;
          tabs = 4;

          show-actions = true;
        };

        colors = {
          text = "${theme.colours.normal.white}ff";
          selection-text = "${theme.colours.normal.white}ff";
          match = "${theme.colours.light.white}ff";
          selection-match = "${theme.colours.light.white}ff";
          prompt = "${theme.colours.normal.white}ff";
          input = "${theme.colours.normal.white}ff";

          selection = "${theme.accent.primary}ff";
          background = "${theme.background.primary}ff";
          border = "${theme.accent.primary}ff";
        };

        border = {
          width = theme.desktop.border-width;
          radius = theme.desktop.corner-radius;
        };
      };
    };
  };
}
