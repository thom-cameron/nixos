{
  lib,
  config,
  theme,
  ...
}:
{
  options.personal.environment.mako.enable = lib.mkEnableOption "enable mako";

  config = lib.mkIf config.personal.environment.mako.enable {
    services.mako = {
      enable = true;

      # aesthetics
      backgroundColor = "#${theme.background.primary}";
      borderColor = "#${theme.accent.primary}";
      progressColor = "#${theme.background.secondary}";
      font = "${theme.font.mono.name} 10";
      borderSize = theme.desktop.border-width;
      borderRadius = theme.desktop.corner-radius;

      # behaviour
      sort = "-time";
      maxVisible = 10;
      layer = "overlay";
      anchor = "top-right";

      # timing
      defaultTimeout = 10000;
      ignoreTimeout = false;

      extraConfig = # ini
        ''
          [urgency=high]
          border-color=#${theme.colours.normal.red}
          default-timeout=0

          [mode=do-not-disturb]
          invisible=1
        '';
    };
  };
}
