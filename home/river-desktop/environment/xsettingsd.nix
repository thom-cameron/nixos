{
  lib,
  config,
  theme,
  ...
}:
{
  options.personal.environment.xsettingsd.enable = lib.mkEnableOption "enable xsettingsd";

  config = lib.mkIf config.personal.environment.xsettingsd.enable {
    services.xsettingsd = {
      enable = true;

      settings = {
        "Net/ThemeName" = theme.gtk.theme.name;
        "Net/IconThemeName" = theme.gtk.theme.name;
        "Gtk/CursorThemeName" = theme.cursor.name;
        "Net/EnableEventSounds" = true;
        "EnableInputFeedbackSounds" = false;
        "Xft/Antialias" = true;
        "Xft/Hinting" = true;
        "Xft/HintStyle" = "hintmedium";
        "Xft/RGBA" = "rgb";
      };
    };
  };
}
