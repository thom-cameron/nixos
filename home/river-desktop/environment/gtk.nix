{
  lib,
  config,
  theme,
  ...
}:
{
  options.personal.environment.gtk.enable = lib.mkEnableOption "configure gtk";

  config = lib.mkIf config.personal.environment.gtk.enable {
    dconf.enable = true;

    gtk = {
      enable = true;

      theme = {
        package = theme.gtk.theme.package;
        name = theme.gtk.theme.name;
      };

      iconTheme = {
        package = theme.gtk.icon-theme.package;
        name = theme.gtk.icon-theme.name;
      };

      cursorTheme = {
        package = theme.cursor.package;
        name = theme.cursor.name;
        size = theme.cursor.size;
      };

      gtk2.extraConfig = # ini
        ''
          gtk-enable-animations=false
        '';
      gtk3.extraConfig = {
        gtk-enable-animations = false;
      };
      gtk4.extraConfig = {
        gtk-enable-animations = false;
      };
    };
  };
}
