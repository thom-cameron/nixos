{
  lib,
  config,
  pkgs,
  ...
}:
let
  sleep_timeout_s = 30 * 60;
  warning_s = 30;
  warning_timeout_s = sleep_timeout_s - warning_s;
in
{
  options.personal.environment.swayidle.enable = lib.mkEnableOption "enable swayidle";

  config = lib.mkIf config.personal.environment.swayidle.enable {
    services.swayidle = {
      enable = true;

      timeouts = [
        {
          timeout = warning_timeout_s;
          command = # bash
            "${pkgs.libnotify}/bin/notify-send swayidle 'suspending in ${toString warning_s}s'";
        }
        {
          timeout = sleep_timeout_s;
          command = # bash
            "${pkgs.systemd}/bin/systemctl suspend";
        }
      ];
    };
  };
}
