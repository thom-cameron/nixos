{
  lib,
  config,
  theme,
  ...
}:
{
  options.personal.environment.i3bar-river.enable = lib.mkEnableOption "enable i3bar-river";

  config = lib.mkIf config.personal.environment.i3bar-river.enable {
    home.file.".config/i3bar-river/config.toml" = {
      text = # toml
        ''
          # colours
          background = "#${theme.background.primary}00"
          color = "#${theme.colours.light.white}ff"
          separator = "#${theme.background.primary}00"
          tag_focused_fg = "#${theme.colours.light.white}ff"
          tag_focused_bg = "#${theme.accent.primary}ff"
          tag_fg = "#${theme.colours.normal.white}ff"
          tag_bg = "#${theme.background.secondary}ff"
          tag_urgent_fg = "#${theme.colours.light.white}ff"
          tag_urgent_bg = "#${theme.colours.normal.red}ff"
          tag_inactive_fg = "#${theme.colours.normal.white}ff"
          tag_inactive_bg = "#${theme.background.secondary}ff"

          # font and sizes
          font = "${theme.font.mono.name} 10.5"
          height = 22
          margin_top =  ${toString theme.desktop.padding}
          margin_bottom = 0
          margin_left = 0
          margin_right = 0
          tags_r = ${toString theme.desktop.corner-radius}
          blocks_r = ${toString theme.desktop.corner-radius}
          tags_padding = 14
          separator_width = 0
          blocks_overlap = 0
          tags_margin = 8

          # misc
          position = "bottom"
          show_layout_name = false
          blend = false
          layer = "top"

          # command
          command = "i3status-rs $HOME/.config/i3status-rust/config-bottom.toml"
        '';
    };
  };
}
