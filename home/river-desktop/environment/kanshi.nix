{
  lib,
  config,
  ...
}:
let
  laptop_display = "eDP-1";
in
{
  options.personal.environment.kanshi.enable = lib.mkEnableOption "enable kanshi";

  config = lib.mkIf config.personal.environment.kanshi.enable {
    services.kanshi = {
      enable = true;

      settings = [
        {
          profile = {
            name = "home-office-desk";
            outputs = [
              {
                criteria = "Ancor Communications Inc ASUS PB278 EBLMTF058794";
                status = "enable";
                position = "0,0";
              }
              {
                criteria = laptop_display;
                status = "disable";
              }
            ];
            exec = ''notify-send kanshi "output profile 'home-office-desk' applied"'';
          };
        }

        {
          profile = {
            name = "home-standing-desk";
            outputs = [
              {
                criteria = "Dell Inc. DELL E2720H 6GP3G43";
                status = "enable";
                position = "0,0";
              }
              {
                criteria = laptop_display;
                status = "enable";
                position = "0,1080";
              }
            ];
            exec = ''notify-send kanshi "output profile 'home-standing-desk' applied"'';
          };
        }

        {
          profile = {
            name = "laptop";
            outputs = [
              {
                criteria = laptop_display;
                status = "enable";
                position = "0,0";
              }
            ];
            exec = ''notify-send kanshi "output profile 'laptop-only' applied"'';
          };
        }
      ];
    };
  };
}
