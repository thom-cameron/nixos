{ user, ... }:
{
  # home-manager
  home.stateVersion = "24.11";
  programs.home-manager.enable = true;

  # user
  home.username = user.name;
  home.homeDirectory = "/home/${user.name}";

  # install packages and shortcuts
  imports = [
    # common
    ../common/common.nix

    # system-specific
    ./environment/environment.nix
    ./shortcuts/shortcuts.nix
    ./packages.nix
  ];

  personal = {
    # environment
    environment = {
      river.enable = true;
      kanshi.enable = true;
      i3bar-river.enable = true;
      i3status-rust.enable = true;
      mako.enable = true;
      fuzzel.enable = true;
      darkman.enable = true;
      swayidle.enable = true;
      udiskie.enable = true;
      cursor.enable = true;
      gtk.enable = true;
      xsettingsd.enable = true;
      desktop-portal.enable = true;
    };

    # utilities
    utilities = {
      alacritty.enable = true;
      nushell.enable = true;
      helix.enable = true;
      git.enable = true;
      gitui.enable = true;
      isw.enable = true;
      bat.enable = true;
      fastfetch.enable = true;
      ruff.enable = true;
      yazi.enable = true;
    };

    # media
    media = {
      mpv.enable = true;
      ncpamixer.enable = true;
    };

    # web
    web = {
      firefox.librewolf.enable = true;
    };

    # office
    office = {
      pandoc.enable = true;
      teams-for-linux.enable = true;
    };

    # shortcuts
    shortcuts = {
      # common shortcuts
      colour-picker.enable = true;
      config.enable = true;
      note.enable = true;
      ocr.enable = true;
      power.enable = true;
      rebuild-system.enable = true;
      rebuild-user.enable = true;
      screenshot.enable = true;
      screen-record.enable = true;
      shortcut.enable = true;
      symbol.enable = true;
      update-packages.enable = true;
      webcam-preview.enable = true;

      # specific shortcuts
      picture-in-picture.enable = true;

      # common config scripts
      empty-trash.enable = true;
    };

    # default applications
    default-apps.enable = true;
  };

  # services
  systemd.user.startServices = true;
}
