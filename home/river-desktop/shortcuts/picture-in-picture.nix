{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.picture-in-picture.enable = lib.mkEnableOption "enable picture-in-picture";

  config = lib.mkIf config.personal.shortcuts.picture-in-picture.enable {
    home.file.".local/bin/shortcuts/picture-in-picture" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Float a view in the corner of the screen and assign to all tags

          all_tags=$(((1 << 32) - 1))
          riverctl move up 0 # set floating
          riverctl snap down
          riverctl snap right
          riverctl set-view-tags $all_tags
        '';
    };
  };
}
