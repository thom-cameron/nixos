{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.office.pandoc.enable = lib.mkEnableOption "enable pandoc";

  config = lib.mkIf config.personal.office.pandoc.enable {
    programs.pandoc = {
      enable = true;

      defaults = {
        metadata = {
          author = "Thom Cameron";
        };
        pdf-engine = "typst";
      };

      templates = { } // import ./templates/typst.nix pkgs;
    };
  };
}
