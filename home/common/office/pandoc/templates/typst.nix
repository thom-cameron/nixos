pkgs: {
  "default.typst" = pkgs.writeText "default.typst" ''
    // Some definitions presupposed by pandoc's typst output.
    #let horizontalrule = [
      #line(start: (25%,0%), end: (75%,0%))
    ]

    #let endnote(num, contents) = [
      #stack(dir: ltr, spacing: 3pt, super[#num], contents)
    ]

    #show terms: it => {
      it.children
        .map(child => [
          #strong[#child.term]
          #block(inset: (left: 1.5em, top: -0.4em))[#child.description]
          ])
        .join()
    }

    $if(template)$
    #import "$template$": conf
    $else$
    $template.typst()$
    $endif$

    #show: doc => conf(
    $if(title)$
      title: [$title$],
    $endif$
    $if(subtitle)$
      subtitle: [$subtitle$],
    $endif$
    $if(author)$
      authors: (
    $for(author)$
    $if(author.name)$
        ( name: [$author.name$],
          affiliation: [$author.affiliation$],
          email: [$author.email$] ),
    $else$
        ( name: [$author$],
          affiliation: "",
          email: "" ),
    $endif$
    $endfor$
        ),
    $endif$
    $if(keywords)$
      keywords: ($for(keywords)$$keyword$$sep$,$endfor$),
    $endif$
    $if(date)$
      date: [$date$],
    $endif$
    $if(lang)$
      lang: "$lang$",
    $endif$
    $if(region)$
      region: "$region$",
    $endif$
    $if(abstract)$
      abstract: [$abstract$],
    $endif$
    $if(margin)$
      margin: ($for(margin/pairs)$$margin.key$: $margin.value$,$endfor$),
    $endif$
    $if(papersize)$
      paper: "$papersize$",
    $endif$
    $if(mainfont)$
      font: ("$mainfont$",),
    $endif$
    $if(fontsize)$
      fontsize: $fontsize$,
    $endif$
    $if(section-numbering)$
      sectionnumbering: "$section-numbering$",
    $endif$
      cols: $if(columns)$$columns$$else$1$endif$,
      doc,
    )

    $for(header-includes)$
    $header-includes$

    $endfor$
    $for(include-before)$
    $include-before$

    $endfor$
    $if(toc)$
    #outline(
      title: auto,
      depth: $toc-depth$
    );
    $endif$

    $body$

    $if(citations)$
    $if(csl)$

    #set bibliography(style: "$csl$")
    $elseif(bibliographystyle)$

    #set bibliography(style: "$bibliographystyle$")
    $endif$
    $if(bibliography)$

    #bibliography($for(bibliography)$"$bibliography$"$sep$,$endfor$)
    $endif$
    $endif$
    $for(include-after)$

    $include-after$
    $endfor$
  '';

  "template.typst" = pkgs.writeText "template.typst" ''
    #let content-to-string(content) = {
      if content.has("text") {
        content.text
      } else if content.has("children") {
        content.children.map(content-to-string).join("")
      } else if content.has("body") {
        content-to-string(content.body)
      } else if content == [ ] {
        " "
      }
    }
    #let conf(
      title: none,
      subtitle: none,
      authors: (),
      keywords: (),
      date: datetime.today(),
      abstract: none,
      cols: 1,
      margin: (x: 1.5cm, y: 1.5cm),
      paper: "a5",
      lang: "en",
      region: "GB",
      font: ("Inter", "Linux Libertine"),
      fontsize: 10pt,
      sectionnumbering: "1.1",
      doc,
    ) = {
      set document(
        title: title,
        author: authors.map(author => content-to-string(author.name)),
        keywords: keywords,
      )
      set page(
        paper: paper,
        margin: margin,
        numbering: "1",
      )
      set par(justify: true)
      set text(lang: lang,
               region: region,
               font: font,
               size: fontsize)
      set heading(numbering: sectionnumbering)

      if title != none {
        par(justify: false, text(14pt)[*#title*])
        v(-8pt)
        par(
          justify: false,
          text(10pt)[#authors.map(author => author.name).join(", "), #date.display("[day] [month repr:short] [year]")],
        )
        v(8pt)
        outline(
          depth: 2,
          indent: true,
          fill: line(length: 100%, stroke: 0.5pt),
        )
        v(12pt)
      }

      if abstract != none {
        block(inset: 2em)[
        #text(weight: "semibold")[Abstract] #h(1em) #abstract
        ]
      }

      if cols == 1 {
        doc
      } else {
        columns(cols, doc)
      }

      // paragraph
      set par(justify: true)
      // code
      show raw: set text(font: ("CaskaydiaCove NF", "DejaVu Sans Mono"))
      show raw.where(block: true): content => {
        set par(justify: false)
        align(
          center,
          block(
            stroke: 0.5pt,
            inset: 8pt,
            radius: 4pt,
            content,
          ),
        )
      }
      // tables
      set table(align: left, stroke: 0.5pt, inset: 4pt)
      show table: table => [#align(center, table)]
      show table.cell: content => [
        #set text(size: 9pt, hyphenate: false)
        #set par(justify: false)
        #content
      ]
      show table.cell.where(y: 0): strong
      // links
      show link: underline
      // lists
      set list(marker: ("•"), indent: 8pt)

    }
  '';
}
