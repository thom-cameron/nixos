{
  lib,
  config,
  ...
}:
{
  options.personal.office.teams-for-linux.enable = lib.mkEnableOption "enable teams-for-linux";

  config = lib.mkIf config.personal.office.teams-for-linux.enable {
    home.file.".config/teams-for-linux/config.json" = {
      text = # json
        ''
          {
            "notificationMethod": "electron",
            "defaultNotificationUrgency": "normal",

            "followSystemTheme": true,

            "appIdleTimeout": 1e6,
            "appIdleTimeoutCheckInterval": 1e6
          }
        '';
    };
  };
}
