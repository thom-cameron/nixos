{
  lib,
  config,
  ...
}:
let
  text_editor = "Helix.desktop";
  image_viewer = "org.gnome.Loupe.desktop";
  video_player = "mpv.desktop";
  web_browser = "librewolf.desktop";
  pdf_viewer = "org.gnome.Papers.desktop";
in
{
  options.personal.default-apps.enable = lib.mkEnableOption "configure default-apps";

  config = lib.mkIf config.personal.default-apps.enable {
    xdg.mimeApps = {
      enable = true;

      defaultApplications = {
        # text
        "text/plain" = text_editor;
        "text/markdown" = text_editor;

        # images
        "image/jpeg" = image_viewer;
        "image/png" = image_viewer;
        "image/tiff" = image_viewer;

        # video
        "video/mp4" = video_player;
        "video/mkv" = video_player;

        # audio
        "audio/mpeg" = video_player;
        "audio/ogg" = video_player;
        "audio/wav" = video_player;
        "audio/aac" = video_player;
        "audio/flac" = video_player;

        # browser
        "text/html" = web_browser;
        "x-scheme-handler/http" = web_browser;
        "x-scheme-handler/https" = web_browser;
        "x-scheme-handler/about" = web_browser;
        "x-scheme-handler/unknown" = web_browser;

        # documents
        "application/pdf" = pdf_viewer;

        # specific apps
        "x-scheme-handler/msteams" = "teams-for-linux.desktop";
      };
    };

    home.preferXdgDirectories = true;
  };
}
