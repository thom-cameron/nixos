{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.empty-trash.enable = lib.mkEnableOption "enable empty-trash";

  config = lib.mkIf config.personal.shortcuts.empty-trash.enable {
    home.file.".local/bin/config_scripts/empty-trash" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Empty the freedesktop standard trash folder

          FOLDER_PATH="$XDG_DATA_HOME/Trash"

          if [ -d "$FOLDER_PATH" ]; then
            if cd "$FOLDER_PATH"; then
              rm -rf ./*
            fi
          fi
        '';
    };
  };
}
