{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.colour-picker.enable = lib.mkEnableOption "enable colour-picker";

  config = lib.mkIf config.personal.shortcuts.colour-picker.enable {
    home.file.".local/bin/shortcuts/colour-picker" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Open a colour picker with the value of the pixel clicked next

          ${pkgs.hyprpicker}/bin/hyprpicker --no-fancy --autocopy
        '';
    };
  };
}
