{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.screenshot.enable = lib.mkEnableOption "enable screenshot";

  config = lib.mkIf config.personal.shortcuts.screenshot.enable {
    home.file.".local/bin/shortcuts/screenshot" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Offer options for and take a screenshot with grim

          options='output\nregion'
          filename=$(date +'%d%b%Y-%H%M%S.png')

          case $(echo -e $options | $MENU) in
              output) slurp_args="-o";;
              region) slurp_args="";;
              *) slurp_args="";;
          esac
          ${pkgs.grim}/bin/grim \
              -g "$(${pkgs.slurp}/bin/slurp $slurp_args)" \
              /tmp/$filename

          cat /tmp/$filename | wl-copy
          mkdir -p $XDG_SCREENSHOTS_DIR
          mv /tmp/$filename $XDG_SCREENSHOTS_DIR/$filename
        '';
    };
  };
}
