{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.screen-record.enable = lib.mkEnableOption "enable screen-record";

  config = lib.mkIf config.personal.shortcuts.screen-record.enable {
    home.file.".local/bin/shortcuts/screen-record" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Offer options for and take a screen recording with wf-recorder

          options='output\nregion'
          trim_duration=2
          temp_prefix='temp-'

          # create unique filename
          filename=$(date +'%d%b%Y-%H%M%S.mp4')
          filepath="$XDG_SCREENSHOTS_DIR/$filename"
          temp_filepath="$XDG_SCREENSHOTS_DIR/$temp_prefix$filename"

          # get the area to record
          case $(echo -e $options | $MENU) in
            "region")
              geometry="$(${pkgs.slurp}/bin/slurp)"
              ;;
            "output")
              geometry="$(${pkgs.slurp}/bin/slurp -o)"
              ;;
          esac

          # show what's going on while recording
          $TERMINAL -e ${pkgs.wf-recorder}/bin/wf-recorder -f $filepath -g "$geometry"

          # trim the start and end of the video
          length=$(ffprobe -i $filepath -show_entries format=duration -v quiet -of csv="p=0")
          length_int=''${length%.*}
          ${pkgs.ffmpeg}/bin/ffmpeg \
            -ss $trim_duration \
            -to $(($length_int - $trim_duration)) \
            -i $filepath \
            -vcodec copy \
            -acodec copy \
            "$temp_filepath"
          mv "$temp_filepath" "$filepath"
        '';
    };
  };
}
