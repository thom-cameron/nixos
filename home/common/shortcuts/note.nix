{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.note.enable = lib.mkEnableOption "enable note";

  config = lib.mkIf config.personal.shortcuts.note.enable {
    home.file.".local/bin/shortcuts/note" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Open an editor window to create an nb note

          $TERMINAL -e ${pkgs.nb}/bin/nb add
        '';
    };
  };
}
