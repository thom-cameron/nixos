{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.ocr.enable = lib.mkEnableOption "enable ocr";

  config = lib.mkIf config.personal.shortcuts.ocr.enable {
    home.file.".local/bin/shortcuts/ocr" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Screenshot a region and read the text into the clipboard

          filename=$(date +'%d%b%Y-%H%M%S.png')

          ${pkgs.grim}/bin/grim \
              -g "$(${pkgs.slurp}/bin/slurp)" \
              /tmp/$filename

          ${pkgs.tesseract}/bin/tesseract /tmp/$filename - | wl-copy
          rm /tmp/$filename
        '';
    };
  };
}
