{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.rebuild-system.enable = lib.mkEnableOption "enable rebuild-system";

  config = lib.mkIf config.personal.shortcuts.rebuild-system.enable {
    home.file.".local/bin/shortcuts/rebuild-system" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Rebuild the NixOS system

          $TERMINAL --hold -e ${pkgs.nh}/bin/nh os switch
        '';
    };
  };
}
