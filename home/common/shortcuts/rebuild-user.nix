{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.rebuild-user.enable = lib.mkEnableOption "enable rebuild-user";

  config = lib.mkIf config.personal.shortcuts.rebuild-user.enable {
    home.file.".local/bin/shortcuts/rebuild-user" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Rebuild the user profile with Home Manager

          $TERMINAL --hold -e ${pkgs.nh}/bin/nh home switch
        '';
    };
  };
}
