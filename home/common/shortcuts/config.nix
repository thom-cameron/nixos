{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.config.enable = lib.mkEnableOption "enable config";

  config = lib.mkIf config.personal.shortcuts.config.enable {
    home.file.".local/bin/shortcuts/config" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Open an editor for modifying NixOS configs

          $TERMINAL -e $EDITOR $XDG_CONFIG_HOME/nixos
        '';
    };
  };
}
