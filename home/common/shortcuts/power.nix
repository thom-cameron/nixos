{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.power.enable = lib.mkEnableOption "enable power";

  config = lib.mkIf config.personal.shortcuts.power.enable {
    home.file.".local/bin/shortcuts/power" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Offer power options and execute with systemd

          options='suspend\npoweroff\nreboot'
          systemctl $(echo -e $options | $MENU)
        '';
    };
  };
}
