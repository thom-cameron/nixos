{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.webcam-preview.enable = lib.mkEnableOption "enable webcam-preview";

  config = lib.mkIf config.personal.shortcuts.webcam-preview.enable {
    home.file.".local/bin/shortcuts/webcam-preview" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Allow a webcam to be selected and previewed in mpv

          device=$(find /dev -name 'video*' | $MENU)
          mpv \
          	--demuxer-lavf-format=video4linux2 \
          	--demuxer-lavf-o-set=input_format=mjpeg \
           	--profile=low-latency \
          	--untimed \
          	--vf=hflip \
          	"av://v412:$device"
        '';
    };
  };
}
