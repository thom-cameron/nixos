{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.symbol.enable = lib.mkEnableOption "enable symbol";

  config = lib.mkIf config.personal.shortcuts.symbol.enable {
    home.file.".local/bin/shortcuts/symbol" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Copy an arbitrary unicode symbol to the clipboard

          unipicker --command "$MENU" --copy-command wl-copy
        '';
    };
  };
}
