{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.shortcuts.update-packages.enable = lib.mkEnableOption "enable update-packages";

  config = lib.mkIf config.personal.shortcuts.update-packages.enable {
    home.file.".local/bin/shortcuts/update-packages" = {
      executable = true;
      text = # bash
        ''
          #!${pkgs.bash}/bin/bash
          # Update the packages used in the system/home configuration

          $TERMINAL --hold -e sudo nix flake update "$XDG_CONFIG_HOME/nixos/#"
        '';
    };
  };
}
