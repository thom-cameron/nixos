{
  imports = [
    ./colour-picker.nix
    ./config.nix
    ./empty-trash.nix
    ./note.nix
    ./ocr.nix
    ./power.nix
    ./rebuild-system.nix
    ./rebuild-user.nix
    ./screen-record.nix
    ./screenshot.nix
    ./shortcut.nix
    ./symbol.nix
    ./update-packages.nix
    ./webcam-preview.nix
  ];
}
