{
  lib,
  config,
  ...
}:
let
  settings = {
    # security
    "security.OCSP.require" = false;
    "dom.security.https_only_mode" = true;
    "privacy.resistFingerprinting" = false;
    "privacy.fingerprintingProtection.overrides" = "+AllTargets,-CSSPrefersColorScheme";
    "layout.css.font-visibility" = 1;

    # features
    "extensions.pocket.enabled" = false;
    "browser.tabs.firefox-view" = true;
    "identity.fxaccounts.enabled" = true;
    "browser.formautofill.enabled" = false;
    "browser.formfill.enable" = false;

    # input
    "middlemouse.paste" = false;
    "general.autoScroll" = true;
    "dom.forms.autocomplete.formautofill" = true;
    "browser.ctrlTab.sortByRecentlyUsed" = true;

    # startup
    "browser.startup.homepage" = "chrome://browser/content/blanktab.html";
    "browser.newtabpage.enabled" = false;
    "browser.startup.page" = 3;
    "privacy.clearOnShutdown.history" = false;
    "privacy.clearOnShutdown.downloads" = false;
    "privacy.clearOnShutdown.cookies" = false;

    # region
    "intl.locale.requested" = "en-GB,en-US";
    "intl.regional_prefs.use_os_locales" = true;

    # aesthetics
    "browser.compactmode.show" = true;
    "browser.uidensity" = 1;
    "findbar.highlightAll" = true;
    "browser.urlbar.trimURLs" = true;
    "browser.urlbar.trimHttps" = true;
    "browser.toolbars.bookmarks.visibility" = "newtab";
    "browser.download.autohideButton" = true;
    "ui.prefersReducedMotion" = 1;
  };
in
{
  options.personal.web.firefox = {
    vanilla.enable = lib.mkEnableOption "enable firefox";
    librewolf.enable = lib.mkEnableOption "enable librewolf";
  };

  config = {
    programs.firefox = lib.mkIf config.personal.web.firefox.vanilla.enable {
      enable = true;

      profiles.default = {
        inherit settings;
      };
    };

    programs.librewolf = lib.mkIf config.personal.web.firefox.librewolf.enable {
      enable = true;

      inherit settings;
    };
  };
}
