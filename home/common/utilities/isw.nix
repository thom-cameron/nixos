{
  lib,
  config,
  pkgs,
  ...
}:
{
  options.personal.utilities.isw.enable = lib.mkEnableOption "enable isw";

  config = lib.mkIf config.personal.utilities.isw.enable {
    home.packages = with pkgs; [
      (rustPlatform.buildRustPackage rec {
        pname = "isw";
        version = "0.3.5";

        src = fetchFromGitLab {
          owner = "thom-cameron";
          repo = pname;
          rev = version;
          hash = "sha256-dlQqIF1WvAe9+OjQaKZA5foc56NU22uvzGHAirL0nrA=";
        };

        cargoHash = "sha256-9ZVwDsCnkzKYh08KGslmieTV0Y5GTycYK/0FCE7Ejkw=";

        meta = {
          description = "a simple terminal stopwatch application";
          homepage = "https://gitlab.com/thom-cameron/isw";
          license = lib.licenses.gpl3Only;
          maintainers = with lib.maintainers; [ thom-cameron ];
          mainProgram = "isw";
        };
      })
    ];
  };
}
