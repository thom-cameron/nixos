{
  lib,
  config,
  ...
}:
{
  options.personal.utilities.fastfetch.enable = lib.mkEnableOption "enable fastfetch";

  config = lib.mkIf config.personal.utilities.fastfetch.enable {
    home.file.".config/fastfetch/config.jsonc" = {
      text = # jsonc
        ''
          {
            "logo": {},

            "modules": [
              {"type": "title", "format": "{6}{7}{8}"},

              {"type": "separator", "string": "─"},

              {"type": "os", "format": "{3}"},
              {"type": "kernel", "format": "{1} {2}"},

              "break",

              {"type": "wm", "format": "{2}"},
              {"type": "shell", "format": "{6}"},
              {"type": "terminal", "format": "{5}"},

              "break",

              {"type": "host", "format": "{5} {2} {3}"},
              {"type": "cpu", "format": "{4} x {7}GHz"},
              {"type": "memory", "format": "{2} (using {3})"},

              "break",

              {"type": "media", "format": "{3}, \"{1}\""},
              {"type": "player", "format": "{1}"},

              "break",

              "colors"
            ]
          }
        '';
    };
  };
}
