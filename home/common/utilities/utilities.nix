{
  imports = [
    ./alacritty.nix
    ./bat.nix
    ./fastfetch.nix
    ./nushell/nushell.nix
    ./git.nix
    ./gitui.nix
    ./helix/helix.nix
    ./isw.nix
    ./ruff.nix
    ./yazi/yazi.nix
  ];
}
