let
  rustfmt_options = builtins.concatStringsSep "," [
    "array_width=100"
    "fn_call_width=100"
    "fn_single_line=true"
    "format_strings=true"
    "single_line_if_else_max_width=100"
    "single_line_let_else_max_width=100"
    "wrap_comments=true"
  ];
in
{
  language = [
    {
      name = "markdown";
      language-servers = [
        "marksman"
        "harper"
      ];
    }
    {
      name = "typst";
      language-servers = [
        "tinymist"
        "harper"
      ];
      auto-format = true;
    }
    {
      name = "html";
      language-servers = [
        "vscode-html"
        "harper"
      ];
      auto-format = true;
    }
    {
      name = "python";
      language-servers = [
        "pyright"
        "ruff"
        "harper"
      ];
      auto-format = true;
    }
    {
      name = "rust";
      language-servers = [
        "rust-analyzer"
        "harper"
      ];
      auto-format = true;
      formatter = {
        command = "rustfmt";
        args = [
          "--config"
          rustfmt_options
        ];
      };
    }
    {
      name = "nix";
      language-servers = [
        "nil"
        "harper"
      ];
      auto-format = true;
      formatter = {
        command = "nixfmt";
        args = [ ];
      };
    }
    {
      name = "lua";
      auto-format = true;
      formatter = {
        command = "stylua";
        args = [ "-" ];
      };
    }
  ];

  language-server = {
    harper = {
      command = "harper-ls";
      args = [ "--stdio" ];
      config.harper-ls = {
        linters = {
          avoid_curses = false;
          sentence_capitalization = false;
        };
        codeActions.forceStable = true;
      };
    };

    tinymist = {
      command = "tinymist";
      config = {
        exportPdf = "onSave";
        formatterMode = "typstyle";
        formatterPrintWidth = 100;
      };
    };

    vscode-html = {
      command = "vscode-html-language-server";
    };

    ruff.command = "ruff-lsp";

    rust-analyzer.config = {
      check = {
        command = "clippy";
      };
    };
  };
}
