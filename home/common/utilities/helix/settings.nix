base_theme: {
  theme = "${base_theme}+";

  # editor settings
  editor = {
    line-number = "relative";
    cursorline = true;
    auto-save = true;
    true-color = true;
    color-modes = true;
    scrolloff = 3;

    lsp = {
      display-messages = true;
      display-inlay-hints = true;
    };

    cursor-shape = {
      normal = "block";
      insert = "bar";
      select = "block";
    };

    statusline = {
      mode.normal = "n";
      mode.insert = "i";
      mode.select = "s";
      left = [
        "mode"
        "spinner"
        "selections"
      ];
      center = [
        "file-base-name"
        "read-only-indicator"
        "version-control"
      ];
      right = [
        "position"
        "position-percentage"
        "spacer"
        "diagnostics"
      ];
    };

    soft-wrap = {
      enable = true;
      wrap-at-text-width = true;
      wrap-indicator = "";
    };
    text-width = 99;
    rulers = [ 100 ];

    indent-guides.render = true;

    shell = [
      "bash"
      "-c"
    ];
  };

  # custom mappings
  keys = {
    normal = {
      # paragraph jumping like vim
      "{" = "goto_prev_paragraph";
      "}" = "goto_next_paragraph";
      # move around whole words
      C-left = "move_prev_word_start";
      C-right = "move_next_word_start";
      # quickly navigate the jumplist
      C-j = "save_selection";
      g = {
        j = "jump_forward";
        J = "jump_backward";
      };
      # save and close the file
      C-s = ":write";
      C-q = ":quit";
    };

    insert = {
      # move around whole words
      C-backspace = "delete_word_backward";
      C-left = "move_prev_word_start";
      C-right = "move_next_word_start";
      # quickly navigate the jumplist
      C-j = "save_selection";
      # save the file
      C-s = [
        ":write"
        "normal_mode"
      ];
    };

    select = {
      # paragraph jumping like vim
      "{" = [
        "extend_to_line_bounds"
        "goto_prev_paragraph"
      ];
      "}" = [
        "extend_to_line_bounds"
        "goto_next_paragraph"
      ];
      "]" = {
        j = "jump_forward";
      };
      # quickly navigate the jumplist
      C-j = "save_selection";
      g = {
        j = "jump_forward";
        J = "jump_backward";
      };
      # save the file
      C-s = ":write";
    };
  };
}
