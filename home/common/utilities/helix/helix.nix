{
  lib,
  config,
  theme,
  ...
}:
let
  base_theme = "amberwood";
in
{
  options.personal.utilities.helix.enable = lib.mkEnableOption "enable helix";

  config = lib.mkIf config.personal.utilities.helix.enable {
    programs.helix = {
      enable = true;

      settings = import ./settings.nix base_theme;

      languages = import ./languages.nix;

      themes = {
        "${base_theme}+" = {
          inherits = base_theme;
          "ui.background" = {
            bg = "#${theme.background.primary}";
          };
          "ui.background.separator" = {
            fg = "#${theme.colours.normal.white}";
          };
          "ui.text" = "#${theme.colours.normal.white}";
        };
      };
    };
  };
}
