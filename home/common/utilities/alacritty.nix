{
  lib,
  config,
  pkgs,
  theme,
  ...
}:
{
  options.personal.utilities.alacritty.enable = lib.mkEnableOption "enable alacritty";

  config = lib.mkIf config.personal.utilities.alacritty.enable {
    programs.alacritty = {
      enable = true;

      settings = {
        window = {
          title = "terminal";
          dynamic_title = false;
        };

        cursor = {
          style = {
            shape = "Beam";
            blinking = "On";
          };
          vi_mode_style = {
            shape = "Block";
            blinking = "Off";
          };
        };

        font = {
          size = 11;
          normal = {
            family = "CaskaydiaCove NF";
            style = "Regular";
          };
          bold = {
            family = "CaskaydiaCove NF";
            style = "Bold";
          };
          italic = {
            family = "CaskaydiaCove NF";
            style = "Italic";
          };
          bold_italic = {
            family = "CaskaydiaCove NF";
            style = "Bold Italic";
          };
          builtin_box_drawing = true;
        };

        colors = {
          primary = {
            foreground = "#${theme.colours.normal.white}";
            background = "#${theme.background.primary}";
          };

          normal = {
            black = "#${theme.colours.normal.black}";
            red = "#${theme.colours.normal.red}";
            green = "#${theme.colours.normal.green}";
            yellow = "#${theme.colours.normal.yellow}";
            blue = "#${theme.colours.normal.blue}";
            magenta = "#${theme.colours.normal.magenta}";
            cyan = "#${theme.colours.normal.cyan}";
            white = "#${theme.colours.normal.white}";
          };
        };

        keyboard.bindings = [
          {
            mods = "Control";
            key = "Backspace";
            chars = "\\u0017";
          }
          {
            key = "N";
            mods = "Control";
            action = "SpawnNewInstance";
          }
          {
            key = "Escape";
            mods = "Control";
            mode = "~Vi";
            action = "ToggleViMode";
          }

        ];
      };
    };
  };
}
