{
  lib,
  config,
  user,
  ...
}:
{
  options.personal.utilities.git.enable = lib.mkEnableOption "enable git";

  config = lib.mkIf config.personal.utilities.git.enable {
    programs.git = {
      enable = true;

      userName = user.fullName;
      userEmail = user.email;

      extraConfig = {
        credential = {
          helper = "store";
        };
      };

      delta = {
        enable = true;

        options = {
          # output
          line-numbers = true;
          keep-plus-minus-markers = true;

          # appearance
          theme = "ansi";
          file-decoration-style = "blue box";
        };
      };
    };
  };
}
