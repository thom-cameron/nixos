{
  lib,
  config,
  ...
}:
{
  options.personal.utilities.bat.enable = lib.mkEnableOption "enable bat";

  config = lib.mkIf config.personal.utilities.bat.enable {
    programs.bat = {
      enable = true;

      config.theme = "ansi";
    };
  };
}
