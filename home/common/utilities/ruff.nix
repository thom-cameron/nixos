{
  lib,
  config,
  ...
}:
{
  options.personal.utilities.ruff.enable = lib.mkEnableOption "enable ruff";

  config = lib.mkIf config.personal.utilities.ruff.enable {
    programs.ruff = {
      enable = true;

      settings = {
        line-length = 99;
        indent-width = 4;

        format = {
          quote-style = "single";
        };
      };
    };
  };
}
