local function setup()
	-- customize mode indicator
	Status.mode = function(self)
		local mode = tostring(self._tab.mode):sub(1, 1):lower()

		local style = self:style()
		return ui.Line({
			ui.Span(THEME.status.separator_open):fg(style.bg),
			ui.Span(" " .. mode .. " "):style(style),
			ui.Span(THEME.status.separator_close):fg(style.bg):bg(THEME.status.separator_style.fg),
		})
	end
end

return { setup = setup }
