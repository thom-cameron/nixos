{
  lib,
  config,
  pkgs,
  theme,
  ...
}:
let
  # create custom bookmark keybindings
  bookmarks = {
    "/" = "/";
    h = "~";
    d = "~/downloads";
    D = "~/documents";
    r = "~/documents/remote";
    n = "~/documents/remote/notes";
    c = "~/.config";
    l = "~/.local";
    p = "~/pictures";
    P = "~/projects";
    M = "/run/media";
    N = "~/.config/nixos";
  };
  bookmark-bindings =
    map
      (bookmark: {
        on = [
          "'"
          "${bookmark.key}"
        ];
        run = ''cd "${bookmark.dir}"'';
        desc = "${bookmark.dir}";
      })
      (
        builtins.attrValues (
          lib.concatMapAttrs (key: dir: {
            "${key}" = {
              key = key;
              dir = dir;
            };
          }) bookmarks
        )
      );

  yazi-plugin-repo = pkgs.fetchFromGitHub {
    owner = "yazi-rs";
    repo = "plugins";
    rev = "49137feda8e140ebd7870292030d89c221cacce8";
    sha256 = "sha256-3F7RIg2CZH/jo+XhG0n4Zfspgi/77Hve421j0p3Og+Q=";
  };
in
{
  options.personal.utilities.yazi.enable = lib.mkEnableOption "enable yazi";

  config = lib.mkIf config.personal.utilities.yazi.enable {
    programs.yazi = {
      enable = true;

      settings = {
        manager = {
          ratio = [
            1
            2
            2
          ];
          sort_dir_first = true;
          show_symlink = false;
        };

        preview = {
          wrap = "yes";
        };
      };

      theme = {
        manager = {
          cwd.fg = "#${theme.colours.normal.blue}";
        };

        status = {
          separator_open = "";
          separator_close = "";
          separator_style = {
            fg = "reset";
            bg = "#${theme.background.secondary}";
          };
        };
      };

      keymap = {
        manager.prepend_keymap = [
          # shell
          {
            on = "!";
            run = "shell --interactive";
            desc = "Run a shell command";
          }
          {
            on = "w";
            run = "shell $SHELL --block --confirm";
            desc = "Open shell here";
          }

          # navigating directories
          {
            on = "k";
            run = "plugin --sync arrow --args=-1";
            desc = "Navigate up (wrapping)";
          }
          {
            on = "j";
            run = "plugin --sync arrow --args=1";
            desc = "Navigate down (wrapping)";
          }
          {
            on = "f";
            run = "plugin jump-to-char";
            desc = "Jump to char";
          }
          {
            on = "F";
            run = "filter --smart";
            desc = "Filter files";
          }
          {
            on = [
              "g"
              "e"
            ];
            run = "arrow 99999999";
            desc = "Move cursor to the bottom";
          }

          # selection
          {
            on = "s";
            run = [
              "select --state=none"
              "arrow 1"
            ];
            desc = "Toggle the current selection state";
          }
          {
            on = "<Esc>";
            run = [
              "escape"
              "select_all --state=false"
              "unyank"
            ];
            desc = "Exit modes and inputs and deselect all";
          }
          {
            on = "<C-a>";
            run = "select_all --state=true";
            desc = "Select all";
          }

          # open in apps
          {
            on = "e";
            run = ''shell '$EDITOR "$@"' --block --confirm'';
            desc = "Open in text editor";
          }
          {
            on = "G";
            run = "shell gitui --block --confirm";
            desc = "Open gitui";
          }

          # tab controls
          {
            on = "<C-t>";
            run = "tab_create --current";
            desc = "Create a new tab with CWD";
          }
          {
            on = "<C-w>";
            run = "close";
            desc = "Close the current tab; or quit if it's last";
          }

          # task management
          {
            on = "t";
            run = "tasks_show";
            desc = "Show task manager";
          }

          # space menu
          {
            on = [
              "<Space>"
              "f"
            ];
            run = "plugin zoxide";
            desc = "Jump to a directory via zoxide";
          }
          {
            on = [
              "<Space>"
              "r"
            ];
            run = "search fd";
            desc = "Search files by name via fd";
          }
          {
            on = [
              "<Space>"
              "R"
            ];
            run = "search rg";
            desc = "Search files by content via ripgrep";
          }
          {
            on = [
              "<Space>"
              "g"
            ];
            run = ''shell 'xdg-open "$(dirname "$1")"' --orphan --confirm'';
            desc = "Open directory in graphical explorer";
          }

          # create new files and directories
          {
            on = [
              "<Space>"
              "n"
            ];
            run = "create";
            desc = "Create a file (ends with / for directories)";
          }
        ] ++ bookmark-bindings;

        input.prepend_keymap = [
          {
            on = "<Esc>";
            run = "close";
            desc = "Cancel input";
          }
        ];
      };

      enableNushellIntegration = true;

      initLua = # lua
        ''
          -- ui tweaks
          require("full-border"):setup {
          	type = ${if theme.desktop.corner-radius > 0 then "ui.Border.ROUNDED" else "ui.Border.PLAIN"},
          }
          require("lowercase-mode"):setup()
          require("dynamic-columns"):setup()
          require("detailed-path"):setup()
          require("full-prompt"):setup()
        '';

      plugins = {
        # navigation
        arrow = ./plugins/arrow.yazi;
        jump-to-char = "${yazi-plugin-repo}/jump-to-char.yazi";

        # ui
        full-border = "${yazi-plugin-repo}/full-border.yazi";
        lowercase-mode = ./plugins/lowercase-mode.yazi;
        dynamic-columns = ./plugins/dynamic-columns.yazi;
        detailed-path = ./plugins/detailed-path.yazi;
        full-prompt = ./plugins/full-prompt.yazi;
      };
    };
  };
}
