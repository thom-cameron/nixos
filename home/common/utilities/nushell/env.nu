# prompt
def create_left_prompt [] {
    let dir = match (do --ignore-shell-errors { $env.PWD | path relative-to $nu.home-path }) {
        null => $env.PWD
        '' => '~'
        $relative_pwd => ($relative_pwd | path basename)
    }
    let path_segment = $"(ansi blue)($dir)(ansi reset)"

    $"($path_segment)\n\n"
}

def create_right_prompt [] {
    let time_segment = $"(ansi grey)(date now | format date '%X')(ansi reset)"

    let duration = if $env.CMD_DURATION_MS == "0823" {
        ""
    } else if ($env.CMD_DURATION_MS | str length) >= 4 {
        $"(($env.CMD_DURATION_MS | into int) / 1000 | math round -p 1)s"
    } else {
        $"($env.CMD_DURATION_MS)ms"
    }
    let duration_segment = $"(ansi magenta_dimmed)($duration)(ansi reset)"

    let last_exit_code = if ($env.LAST_EXIT_CODE != 0) {([
        (ansi red)
        ($env.LAST_EXIT_CODE)
        (ansi reset)
    ] | str join)
    } else { "" }

    [$last_exit_code $duration_segment $time_segment] | where {|item| $item != ""} | str join " "
}

$env.PROMPT_COMMAND = {|| create_left_prompt }
$env.PROMPT_COMMAND_RIGHT = {|| create_right_prompt }
$env.PROMPT_INDICATOR = {|| "> " }
$env.PROMPT_INDICATOR_VI_INSERT = {|| ": " }
$env.PROMPT_INDICATOR_VI_NORMAL = {|| "> " }
$env.PROMPT_MULTILINE_INDICATOR = {|| "::: " }

# variables
$env.ENV_CONVERSIONS = {
    "PATH": {
        from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
        to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
    }
    "Path": {
        from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
        to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
    }
}
$env.PATH = ($env.PATH | split row (char esep) | prepend '/opt/homebrew/bin')

$env.NU_LIB_DIRS = [
    ($nu.default-config-dir | path join 'scripts') # add <nushell-config-dir>/scripts
    ($nu.data-dir | path join 'completions') # default home for nushell completions
]

$env.NU_PLUGIN_DIRS = [
    ($nu.default-config-dir | path join 'plugins') # add <nushell-config-dir>/plugins
]

# zoxide
zoxide init nushell | save -f ~/.zoxide.nu
