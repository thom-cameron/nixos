{
  lib,
  config,
  user,
  ...
}:
{
  options.personal.utilities.nushell.enable = lib.mkEnableOption "enable nushell";

  config = lib.mkIf config.personal.utilities.nushell.enable {
    programs.nushell = {
      enable = true;

      configFile.source = ./config.nu;
      envFile.source = ./env.nu;
    };
  };
}
