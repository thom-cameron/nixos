{
  lib,
  config,
  ...
}:
{
  options.personal.media.ncpamixer.enable = lib.mkEnableOption "enable ncpamixer";

  config = lib.mkIf config.personal.media.ncpamixer.enable {
    home.file.".config/ncpamixer.conf" = {
      text = ''
        "theme" = "user"

        user theme {
           "theme.user.static_bar"             = false
           "theme.user.default_indicator"      = "● "
           "theme.user.bar_style.bg"           = "●"
           "theme.user.bar_style.fg"           = "●"
           "theme.user.bar_style.indicator"    = "●"
           "theme.user.bar_style.top"          = ""
           "theme.user.bar_style.bottom"       = ""
           "theme.user.bar_low.front"          = 0
           "theme.user.bar_low.back"           = -1
           "theme.user.bar_mid.front"          = 0
           "theme.user.bar_mid.back"           = -1
           "theme.user.bar_high.front"         = 0
           "theme.user.bar_high.back"          = -1
           "theme.user.volume_low"             = 2
           "theme.user.volume_mid"             = 3
           "theme.user.volume_high"            = 1
           "theme.user.volume_peak"            = 1
           "theme.user.volume_indicator"       = 15
           "theme.user.selected"               = 4
           "theme.user.default"                = -1
           "theme.user.border"                 = 7
           "theme.user.dropdown.selected_text" = 0
           "theme.user.dropdown.selected"      = 4
           "theme.user.dropdown.unselected"    = -1
        }

        Keybinds {
           "keycode.13"   = "dropdown"        # enter
           "keycode.113"  = "quit"            # q
           "keycode.27"   = "quit"            # escape

           "keycode.100"  = "set_default"     # d
           "keycode.108"  = "volume_up"       # l
           "keycode.104"  = "volume_down"     # h
           "keycode.261"  = "volume_up"       # arrow right
           "keycode.260"  = "volume_down"     # arrow left
           "keycode.109"  = "set_volume_0"    # m
           "keycode.98"   = "set_volume_40"   # b
           "keycode.77"   = "set_volume_100"  # M

           "keycode.107"  = "move_up"         # k
           "keycode.106"  = "move_down"       # j
           "keycode.259"  = "move_up"         # arrow up
           "keycode.258"  = "move_down"       # arrow down
           "keycode.71"   = "move_last"       # G
           "keycode.103"  = "move_first"      # g
           "keycode.338"  = "page_up"         # page up
           "keycode.339"  = "page_down"       # page down

           "keycode.76"   = "tab_next"        # L
           "keycode.72"   = "tab_prev"        # H
           "keycode.49"   = "tab_playback"    # 1
           "keycode.50"   = "tab_recording"   # 2
           "keycode.51"   = "tab_output"      # 3
           "keycode.52"   = "tab_input"       # 4
           "keycode.53"   = "tab_config"      # 5
        }
      '';
    };
  };
}
