{
  lib,
  pkgs,
  config,
  theme,
  ...
}:
let
  uosc_controls = builtins.concatStringsSep "," [
    "menu"

    "gap"

    "prev"
    "items"
    "next"

    "gap"

    "shuffle"
    "loop-playlist"
    "loop-file"

    "space"

    "subtitles"
    "<has_many_audio>audio"
    "<has_many_video>video"
    "<has_many_edition>editions"
    "<stream>stream-quality"

    "gap"

    "fullscreen"
  ];
in
{
  options.personal.media.mpv.enable = lib.mkEnableOption "enable mpv";

  config = lib.mkIf config.personal.media.mpv.enable {
    programs.mpv = {
      enable = true;

      config = {
        save-position-on-quit = true;
        prefetch-playlist = true;

        screenshot-directory = "~/pictures/screenshots";

        osd-font = theme.font.mono.name;
      };

      bindings = {
        # subtitles
        c = "cycle sub";
        C = "cycle sub down";
        "=" = "add sub-scale +0.1";
        "-" = "add sub-scale -0.1";
      };

      scripts = with pkgs.mpvScripts; [
        # ui
        uosc
        thumbfast

        # audio
        mpris
        visualizer
      ];

      scriptOpts = {
        uosc = {
          timeline_size = 40;
          destination_time = "total";

          controls = uosc_controls;
          controls_size = 24;
          volume = "none";

          top_bar = "always";
          top_bar_size = 40;
          top_bar_controls = false;
          top_bar_persistency = "audio";

          animation_duration = 0;
        };

        visualizer = {
          name = "showcqt";
          height = 8;
        };
      };
    };
  };
}
