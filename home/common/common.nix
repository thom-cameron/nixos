{
  imports = [
    media/media.nix
    office/office.nix
    shortcuts/shortcuts.nix
    utilities/utilities.nix
    web/web.nix
    ./default-apps.nix
  ];
}
