{ pkgs, ... }:
rec {
  colours = {
    normal = {
      black = "282828";
      red = "d3524e";
      green = "689d68";
      yellow = "fcca80";
      blue = "6cadd8";
      magenta = "bf83b1";
      cyan = "86c1b9";
      white = "dddddd";
    };
    light = {
      black = "555555";
      # red = "";
      # green = "";
      # yellow = "";
      # blue = "";
      # magenta = "";
      # cyan = "";
      white = "ffffff";
    };
    dark = {
      black = "000000";
      # red = "";
      green = "406340";
      # yellow = "";
      # blue = "";
      magenta = "9183bf";
      # cyan = "";
      white = "888888";
    };
  };

  accent = {
    primary = colours.dark.magenta;
    secondary = colours.normal.magenta;
    tertiary = colours.normal.blue;
  };

  background = {
    primary = colours.dark.black;
    secondary = colours.normal.black;
    tertiary = colours.light.black;
  };

  font = {
    mono = {
      package = pkgs.nerd-fonts.caskaydia-cove;
      name = "CaskaydiaCove NF";
      file = "${pkgs.nerdfonts}/share/fonts/truetype/NerdFonts/CaskaydiaCoveNerdFont-Regular.ttf";
    };

    sans = {
      package = pkgs.inter;
      name = "Inter";
      file = "${pkgs.inter}/share/fonts/truetype/Inter.ttc";
    };
  };

  cursor = {
    package = pkgs.adwaita-icon-theme;
    name = "Adwaita";
    size = 24;
  };

  gtk = {
    theme = {
      package = pkgs.gnome-themes-extra;
      name = "Adwaita";
    };

    icon-theme = {
      package = pkgs.adwaita-icon-theme;
      name = "Adwaita";
    };

    font = {
      package = font.sans.package;
      name = font.sans.name;
      size = 11;
    };
  };

  desktop = {
    padding = 4;
    border-width = 4;
    corner-radius = 4;
  };

  temperature = {
    light = 6500;
    dark = 4500;
  };
}
