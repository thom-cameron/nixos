{
  description = "thom's nixos config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      nixpkgs,
      home-manager,
      ...
    }:
    let
      system = "x86_64-linux";

      pkgs = import nixpkgs {
        inherit system;
        overlays = [ ];
      };

      lib = nixpkgs.lib;

      user = {
        personal = {
          name = "thom";
          fullName = "Thom Cameron";
          email = "thomcm@proton.me";
        };
      };

      theme = import ./theme.nix pkgs;
    in
    {
      nixosConfigurations = {
        "nix-lat" = lib.nixosSystem {
          specialArgs = {
            user = user.personal;
          };
          modules = [ ./system/lat/configuration.nix ];
        };
      };

      homeConfigurations = {
        "thom@nix-lat" = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          extraSpecialArgs = {
            user = user.personal;
            inherit theme;
          };
          modules = [ ./home/river-desktop/home.nix ];
        };
      };
    };
}
